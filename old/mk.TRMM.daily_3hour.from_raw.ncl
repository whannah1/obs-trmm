load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    dir = "~/Data/Obs/TRMM"
    
    idir = dir+"/raw_3hour"
    odir = dir+"/daily_3hour"

        yr1 = 1999
        yr2 = 1999
        
    ivar = "precipitation"
    ovar = "precip"

;===================================================================================
;===================================================================================
  
days_per_month = (/31,28,31,30,31,30,31,31,30,31,30,31/)

print("")
do y = yr1,yr2
do m = 1,12
do d = 1,days_per_month(m-1)
    yr = sprinti("%0.2i",y)
    mn = sprinti("%0.2i",m)
    dy = sprinti("%0.2i",d)
    ofile = odir+"/3b42."+yr+mn+dy+".nc"
    print("  ofile : "+ofile)
    if isfilepresent(ofile) then system("rm "+ofile) end if
    outfile = addfile(ofile,"c")
    outfile@date = ""+yr+mn+dy
    do h = 0,7
        hr = h*3
        ifile = systemfunc("ls "+idir+"/3B42."+yr+mn+dy+"."+hr+"*.nc")
        infile = addfile(ifile,"R")
        if h.eq.0 then
            latsz = dimsizes(infile->lat)
            lonsz = dimsizes(infile->lon)
            precip = new((/8,latsz,lonsz/),float)
            precip!0 = "time"
            precip!1 = "lat"
            precip!2 = "lon"
            precip&lat = infile->lat
            precip&lon = infile->lon
            precip@units = "mm/day"
            precip@long_name = "precipitation"
        end if
        precip(h,:,:) = infile->$ivar$ * 24.
    end do
    outfile->$ovar$ = precip
    delete(precip)
end do
end do
end do
print("")
;===================================================================================
;===================================================================================

end
